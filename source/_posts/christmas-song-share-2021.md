---
title: 圣诞歌曲分享2021
tags: 杂项
categories: 祭坛洞窟
keywords: 圣诞 歌曲分享 Macross
description: Merry christmas!
cover: 'https://storage.live.com/items/441D3FDA5A491ADA!469590:/7.0.webp'
date: 2021-12-25 21:11:14
---


因为之前和圣诞直接相关的歌曲的库存好像用完了，就打算今年分享一些其他歌曲。结果正好前一段时间随机到了这首歌，好，就是你了！

{% iframe https://music.163.com/outchain/player?type=2&id=4931483&auto=1&height=66 [310] [66] %}

之前看Macross F的时候都没有注意到这首歌。今年正好有新的短篇剧场，虽然还没有看到。圣诞快乐！
