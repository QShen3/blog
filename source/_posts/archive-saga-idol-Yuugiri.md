---
title: '[存档]佐贺偶像'
tags:
  - 存档
  - 杂谈
categories: 陆行鸟森
keywords:
  - 动画
  - 佐贺
  - 偶像
  - 夕雾
description: 佐贺事变！
cover: 'https://storage.live.com/items/441D3FDA5A491ADA!468755:/0.jpg'
date: 2021-06-12 00:50:54
---



# 写在前面

　　6月5日的朋友圈

# 全文

　　佐贺偶像作为一个地方宣传整活片，最近两周的佐贺事变篇真的是惊艳，没想到严肃内容也有这么高的质量。作为花魁夕雾的个人回，不仅引入徐福的传说，一定程度上解释了“不死”的设定，还通过类似历史正剧的形式，交代了整个系列的内涵。明治时期，佐贺之乱之后，佐贺被并入长崎，想复兴并建立新佐贺的男主的理想被被乱党利用，卷入了新的叛乱事件。最终被身为陆军间谍的男主的朋友发现并上报朝廷，只有男主一人在花魁夕雾的帮助下逃亡到了长崎，最终夕雾被陆军拿去顶罪，被处以斩首。在真正有志之士的不断推动下，佐贺得以重新立县。虽然说只是看起来像“政治儿戏”的内容，并且只是在真实历史背景下的虚构人物，但是配合演出效果还是挺令人震撼的。毕竟如果不是这部地方振兴宣传片，会有多少普通观众去了解佐贺的地方历史。就这一点来说已经非常成功了。之前不知道是谁最早说的，说这部动画是献给佐贺的情书，现在看确实能感受到，制作组的热情已经快溢出屏幕了

![1](https://storage.live.com/items/441D3FDA5A491ADA!468670:/1.png)
![2](https://storage.live.com/items/441D3FDA5A491ADA!468672:/2.png)
![3](https://storage.live.com/items/441D3FDA5A491ADA!468673:/3.png)
![4](https://storage.live.com/items/441D3FDA5A491ADA!468674:/4.png)
![5](https://storage.live.com/items/441D3FDA5A491ADA!468675:/5.png)
![6](https://storage.live.com/items/441D3FDA5A491ADA!468676:/6.png)
![7](https://storage.live.com/items/441D3FDA5A491ADA!468677:/7.png)
![8](https://storage.live.com/items/441D3FDA5A491ADA!468678:/8.png)
![9](https://storage.live.com/items/441D3FDA5A491ADA!468679:/9.png)
![10](https://storage.live.com/items/441D3FDA5A491ADA!468680:/10.png)

　　顺带一提，虽然是虚构人物，但是夕雾这通天人脉真的是恐怖
![11](https://storage.live.com/items/441D3FDA5A491ADA!468681:/11.png)