---
title: 常规更新与维护 （2020-03-31 更新）
tags: [站务, 维护]
categories: 基萨尔村
keywords: 更新 维护
description: 常规更新与维护
date: 2020-06-30 15:15:00
---

2020-03-31

　　常规更新
1. 博客相关
    - 更新Hexo至5.4.0
    - 更新Hexo-theme-butterfly至3.7.1
    - 更新Node.js至14.16.1
    - 更新Nginx至1.19.10

2. Gitlab相关
    - 更新Gitlab至13.10.2
    - 更新gitlab-runner至13.10.0

3. VPN相关
    - 更新kcptun至20210103

4. 基础架构相关
    - 更新traefik至2.4.8
    - 更新portainer至2.1.1
    - 更新frp至0.36.2

---

2020-12-31

　　尝试了一下iframe插件，看起来也不麻烦

　　常规更新
1. 博客相关
    - 更新Node.js至14.15.3
    - 更新Nginx至1.19.6

2. Gitlab相关
    - 更新Gitlab至13.7.1
    - 更新gitlab-runner至13.6.0

3. VPN相关
    - 更新kcptun至v20201126

4. 基础架构相关
    - 更新frp至0.34.3

---

2020-09-30

　　上一个季度增加了评论功能，看起来运行良好

　　常规更新
1. 博客相关
    - 更新Hexo至5.2.0
    - 更新Hexo-theme-butterfly至3.2.0
    - 更新Node.js至12.18.4
    - 更新Nginx至1.19.2

2. Gitlab相关
    - 更新Gitlab至13.4.1
    - 更新gitlab-runner至13.4.1

3. VPN相关
    - 更新kcptun至20200930

4. 基础架构相关
    - 更新frp至0.34.0

---

2020-06-30

　　本来想的是在第一次常规更新与维护之前，就可以写至少一篇文章，增加至少一个新功能。结果一个都没有完成。文章在写了，目前进度大概60%，预计这两天可以完成，之后会优先完成评论功能。

　　下面是本次常规更新的一些内容
1. 博客相关
    - 更新Hexo至4.0.0
    - 更新Hexo-theme-butterfly至3.0.0-rc1
    - 更新Node.js至12.18.1
    - 更新Nginx至1.19.0

2. Gitlab相关
    - 更新Gitlab至13.1.1
    - 更新gitlab-runner至13.1.0

3. VPN相关
    - 更新kcptun至20200409

　　预计晚上20点开始维护，最晚24点之前结束
