---
title: 圣诞歌曲分享2020
tags: 杂项
categories: 祭坛洞窟
keywords: 圣诞 歌曲分享 蜂蜜与四叶草
description: Merry christmas!
cover: 'https://storage.live.com/items/441D3FDA5A491ADA!467803:/6.0.png'
date: 2020-12-25 00:20:14
---


之前每年圣诞的时候都会在朋友圈分享一首和圣诞有关的歌曲，今年就发到博客里面吧。

{% iframe https://music.163.com/outchain/player?type=2&id=498063&auto=1&height=66 [310] [66] %}

今年的歌曲是《蜂蜜与四叶草》圣诞回的插入曲。这也是我今年补的TV动画里印象最深的。圣诞快乐！
